import uvicorn
#from app.main import logger
#from devtools import debug


def main():
    #logger.info("Running cast-service")
    
    uvicorn.run("app.main:app", host="0.0.0.0",
                port=8000, reload=True, workers=4)


if __name__ == "__main__":
    main()
