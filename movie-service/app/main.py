# https://dev.to/ericchapman/python-fastapi-crash-course-533e


from fastapi import FastAPI

from app.utils.loggerfactory import LoggerFactory
from pydantic import BaseModel
from typing import List
from app.api.movies import movies

from app.api.db import metadata, database, engine
import os

#  manage-fastapi, fastapi-pagination,fastapi-debug-toolbar,fastapi-admin y fastapi-plugins
# fastapi-versioning

logger = LoggerFactory.get_logger("movie-service", log_level="INFO")

app = FastAPI(
    title="My App",
    debug=True,
    openapi_url="/api/v1/movies/openapi.json",
    docs_url="/api/v1/movies/docs",
)


metadata.create_all(engine)


@app.on_event("startup")
async def startup():

    logger.info("Starting up!")
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    logger.info("Shutdown!")
    await database.disconnect()


app.include_router(movies, prefix="/api/v1/movies", tags=["movies"])
