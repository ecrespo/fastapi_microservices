from fastapi import APIRouter, status, Request
from app.utils.loggerfactory import LoggerFactory

tests = APIRouter()
logger = LoggerFactory.get_logger("movie-service", log_level="INFO")


@tests.get("/hello", status_code=status.HTTP_200_OK)
def read_main():
    logger.info("Hello World!")
    return {"msg": "Hello World"}


@tests.get("/run", status_code=status.HTTP_200_OK)
def root(request: Request):
    host = request.headers["host"]
    tenant = host.split(":")[0].split(".")[0]
    return {"Tenant": tenant}
